package com.example.cp.go;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Random;

public final class notificationManager {

    // Documentation for how to implement this class is from the following link:
    // https://developer.android.com/training/notify-user/build-notification
    // Last Access: 9/16/2018

    // The notification channel ID used in android Oreo and above
    private static final String CHANNEL_ID = "CP-default";
    // The activity context that is responsible for creating the notifications
    private static android.content.Context app_context = null;

    // The stated intent to transition from a notification to a notification details screen.
    private static Intent intent;
    // The notification details screen
    private static Activity notificationDetailsScreen;

    // Random number generator used for generating notificationID's
    private static Random rand;

    private static Hashtable<Integer, NotificationCompat.Builder> allNotifications;

    /**
     * Default constructor, logs an error message asking you to please not use the constructor,
     * and to instead use the static method init(), as well as the other static methods provided.
     */
    public notificationManager() throws Exception {
        throw new Exception("Please do not create a notification manager as an " +
        "object, only use the static methods provided. Please remember to use init().");
    }

    /**
     * Initializes the notification manager using a given activity context
     * @param ctx - The activity context that ties together the notifications.
     */
    public static void init(android.content.Context ctx) throws Exception {
        app_context = ctx;
        createNotificationChannel(app_context);
        intent = new Intent(app_context, notificationDetailsScreen.getClass());
        rand = new Random();
    }

    /**
     * Creates a new notification.
     * @param title - The large text of the notification
     * @param body - The smaller detailed text of the notification
     * @return - A unique identifier for this notification.
     */
    public static int createNotification(String title, String body){
        int key = 0;
        try {
            key = createNotification(app_context, title, body);
        }
        catch (Exception e){
            key = 0;
        }
        return key;
    }

    /**
     * Displays a notification to the screen given a specific notification ID.
     * @param notificationID - The notification ID of the notification you would like to display.
     */
    public static void displayNotification(int notificationID) throws Exception {
        if(app_context == null){
            throw new Exception("No notification can be displayed until the Notification Manager" +
                    " has been initialized.");
        }
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(app_context);
        notificationManager.notify(notificationID, allNotifications.get(notificationID).build());

    }

    /**
     * Creates a new notification channel to dispatch notifications with in android Oreo or higher.
     * @param ctx - The activity context that ties together the notifications.
     */
    private static void createNotificationChannel(android.content.Context ctx) throws Exception {
        if(ctx == null){
            throw new Exception("Notification Channel cannot be created. " +
                    "Notification Manager not initialized");
        }

        // Because notification channels are only supported starting in android OREO,
        // It is important that this code only run on android OREA or later.
        // Build.VERSION.SDK_INT gets the android build version of the phone using the app.
        // Build.VERSION_CODES.O gets the android build version of android OREO.
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            String channelName = ctx.getString(R.string.notification_channel_name);
            String channelDescription = ctx.getString(R.string.notification_channel_description);
            // Highest priority is needed to appear on the status bar, make a sound, and turn on
            // the screen as a "heads-up" notification.
            int importance = NotificationManager.IMPORTANCE_MAX;
            @SuppressLint("WrongConstant") NotificationChannel channel =
                    new NotificationChannel("default", channelName, importance);
            channel.setDescription(channelDescription);
            // Register the channel with the OS.
            android.app.NotificationManager manager =
                    ctx.getSystemService(android.app.NotificationManager.class);
            manager.createNotificationChannel(channel);
        }
    }

    /**
     * The function which really handles creating a new notification, and registering it with the
     * anroid OS. The notification manager must be initialized before use.
     * @param ctx - The activity context that ties the notifications together.
     * @param title - The large text of the notification.
     * @param body - The smaller detail text of the notification.
     * @return - A unique notification ID.
     */
    private static int createNotification(android.content.Context ctx, String title, String body) throws Exception {
        if(ctx == null){
            throw new Exception("Notification cannot be built. " +
                    "Notification Manager not initialized");
        }

        NotificationCompat.Builder notif = new NotificationCompat.Builder(ctx, CHANNEL_ID);
        notif.setSmallIcon(R.drawable.notificationIcon);
        notif.setContentTitle(title);
        notif.setContentText(body);
        // Highest priority is needed to appear on the status bar, make a sound, and turn on
        // the screen as a "heads-up" notification.
        notif.setPriority(NotificationCompat.PRIORITY_MAX);

        // Create an explicit intent for the notification
        PendingIntent pi = PendingIntent.getActivity(app_context, 0, intent, 0);
        notif.setContentIntent(pi);

        // Associate this notification with a unique notification ID.
        int key = getUniqueID();
        // Save the relationship between the notification and the key.
        allNotifications.put(key, notif);

        // Send back the key to the programmer requesting the notification.
        return key;
    }

    /**
     * Generates a new unique, non-zero, notification ID.
     * @return A unique, non-zero, notification ID
     */
    private static int getUniqueID(){
        int key = rand.nextInt();
        while(allNotifications.containsKey(key) || key == 0){
            key++;
        }
        return key;
    }
}
